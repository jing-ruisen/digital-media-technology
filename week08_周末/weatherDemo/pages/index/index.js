// pages/index/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    region: ["广东省","广州市","从化区"],
    geo_location:"113.679287,23.632575",
    now:{
      temp:0,
      text:'未知',
      icon:'999',
      humidity:0,
      pressure:0,
      vis:0,
      windDir:0,
      windSpeed:0,
      windScale:0
    }
  },
  /**
   * 更新省市区信息
   */
  regionChange: function(e) {
    this.setData({region: e.detail.value});
    this.location();
    
  },

  location:function(){
    var that = this;
    wx.request({
      url: 'https://restapi.amap.com/v3/geocode/geo?parameters',
      data:{
        key:'1a8b4a8f8eacf6e72af8287289e0e270',
        address:that.data.region[0]+that.data.region[1]+that.data.region[2],
      },
      success:function(res){
        console.log("高德经纬度：",res.data.geocodes[0]['location'])
        that.setData({geo_location:res.data.geocodes[0]['location']})
        console.log(that.data.geo_location)
      }
    })
  },
  /**
   * 获取实况天气数据
   */
  getWeather: function () {
    var that = this;//this不可以直接在wxAPI函数内部使用
    console.log("dsdsddsds",that.data.geo_location)
    wx.request({
      url: 'https://devapi.qweather.com/v7/weather/now',
      data:{
        location:that.data.geo_location,
        key:'bcc9e2300f5945da8dbdf541135ccbb2'
      },
      success:function(res){
        console.log("和风天气提供的参数（菜单）：",res.data.now);
        that.setData({now:res.data.now});
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.location();//更新位置
    this.getWeather();//更新天气  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})