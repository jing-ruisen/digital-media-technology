-------

* 时间：2021-11-17
* 记录人：许智超
* week12（week06学习周）
* 本周参考资源：[http状态码](https://httpstatusdogs.com/)

-------

# 了解 Web 及网络基础



## 1. 使用 HTTP 协议访问 Web





## 2. HTTP 的诞生





## 3. 网络基础 TCP/IP





## 4. 与 HTTP 关系密切的协议 : IP、TCP 和 DNS





## 5. 负责域名解析的 DNS 服务





## 6.  各种协议与 HTTP 协议的关系





## 7. URI 和 URL











